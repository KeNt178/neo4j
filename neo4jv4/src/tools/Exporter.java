package tools;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.neo4j.graphdb.Result;

public class Exporter {
	private Result result;
	private String file = Database.OUTPUT_PATH+"/incoherences_hierarchiques_type";
	private String[] queries = {"MATCH (fu:Fixe)-[:H]->(fd:Fixe)-[:R]->(gd:Global)<-[:R]-(md:Mobile)<-[:H]-(mu:Mobile) WHERE NOT (fu)-[:R]->(:Global) AND NOT (mu)-[:R]->(:Global) RETURN '{\"niveau\":\"'+labels(fu)[1]+'\", \"libelle\":\"'+fu.nom+'\", \"id\":\"'+fu.id+'\", \"ecran\":\"'+labels(fu)[0]+'\"},{\"niveau\":\"'+labels(fd)[1]+'\", \"libelle\":\"'+fd.nom+'\", \"id\":\"'+fd.id+'\", \"ecran\":\"'+labels(fd)[0]+'\"},{\"niveau\":\"'+labels(gd)[1]+'\", \"libelle\":\"'+gd.nom+'\", \"id\":\"'+gd.id+'\", \"ecran\":\"'+labels(gd)[0]+'\"},{\"niveau\":\"'+labels(md)[1]+'\", \"libelle\":\"'+md.nom+'\", \"id\":\"'+md.id+'\", \"ecran\":\"'+labels(md)[0]+'\"},{\"niveau\":\"'+labels(mu)[1]+'\", \"libelle\":\"'+mu.nom+'\", \"id\":\"'+mu.id+'\", \"ecran\":\"'+labels(mu)[0]+'\"}' as nodes, '{\"from\":\"'+fu.id+'\", \"to\":\"'+fd.id+'\", \"relation\":\"H\"},{\"from\":\"'+fd.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+md.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+mu.id+'\", \"to\":\"'+md.id+'\", \"relation\":\"H\"}' as edges",
								"MATCH (gu:Global)<-[:R]-(fu:Fixe)-[:H]->(fd:Fixe)-[:R]->(gd:Global)<-[:R]-(md:Mobile)<-[:H]-(mu:Mobile) WHERE NOT (mu)-[:R]->(:Global) RETURN '{\"niveau\":\"'+labels(gu)[1]+'\", \"libelle\":\"'+gu.nom+'\", \"id\":\"'+gu.id+'\", \"ecran\":\"'+labels(gu)[0]+'\"},{\"niveau\":\"'+labels(fu)[1]+'\", \"libelle\":\"'+fu.nom+'\", \"id\":\"'+fu.id+'\", \"ecran\":\"'+labels(fu)[0]+'\"},{\"niveau\":\"'+labels(fd)[1]+'\", \"libelle\":\"'+fd.nom+'\", \"id\":\"'+fd.id+'\", \"ecran\":\"'+labels(fd)[0]+'\"},{\"niveau\":\"'+labels(gd)[1]+'\", \"libelle\":\"'+gd.nom+'\", \"id\":\"'+gd.id+'\", \"ecran\":\"'+labels(gd)[0]+'\"},{\"niveau\":\"'+labels(md)[1]+'\", \"libelle\":\"'+md.nom+'\", \"id\":\"'+md.id+'\", \"ecran\":\"'+labels(md)[0]+'\"},{\"niveau\":\"'+labels(mu)[1]+'\", \"libelle\":\"'+mu.nom+'\", \"id\":\"'+mu.id+'\", \"ecran\":\"'+labels(mu)[0]+'\"}' as nodes, '{\"from\":\"'+fu.id+'\", \"to\":\"'+gu.id+'\", \"relation\":\"R\"},{\"from\":\"'+fu.id+'\", \"to\":\"'+fd.id+'\", \"relation\":\"H\"},{\"from\":\"'+fd.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+md.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+mu.id+'\", \"to\":\"'+md.id+'\", \"relation\":\"H\"}' as edges",
								"MATCH (gu:Global)<-[:R]-(mu:Mobile)-[:H]->(md:Mobile)-[:R]->(gd:Global)<-[:R]-(fd:Fixe)<-[:H]-(fu:Fixe) WHERE NOT (fu)-[:R]->(:Global) RETURN '{\"niveau\":\"'+labels(gu)[1]+'\", \"libelle\":\"'+gu.nom+'\", \"id\":\"'+gu.id+'\", \"ecran\":\"'+labels(gu)[0]+'\"},{\"niveau\":\"'+labels(mu)[1]+'\", \"libelle\":\"'+mu.nom+'\", \"id\":\"'+mu.id+'\", \"ecran\":\"'+labels(mu)[0]+'\"},{\"niveau\":\"'+labels(md)[1]+'\", \"libelle\":\"'+md.nom+'\", \"id\":\"'+md.id+'\", \"ecran\":\"'+labels(md)[0]+'\"},{\"niveau\":\"'+labels(gd)[1]+'\", \"libelle\":\"'+gd.nom+'\", \"id\":\"'+gd.id+'\", \"ecran\":\"'+labels(gd)[0]+'\"},{\"niveau\":\"'+labels(fd)[1]+'\", \"libelle\":\"'+fd.nom+'\", \"id\":\"'+fd.id+'\", \"ecran\":\"'+labels(fd)[0]+'\"},{\"niveau\":\"'+labels(fu)[1]+'\", \"libelle\":\"'+fu.nom+'\", \"id\":\"'+fu.id+'\", \"ecran\":\"'+labels(fu)[0]+'\"}' as nodes, '{\"from\":\"'+mu.id+'\", \"to\":\"'+gu.id+'\", \"relation\":\"R\"},{\"from\":\"'+mu.id+'\", \"to\":\"'+md.id+'\", \"relation\":\"H\"},{\"from\":\"'+md.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+fd.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+fu.id+'\", \"to\":\"'+fd.id+'\", \"relation\":\"H\"}' as edges",
								"MATCH (gu1:Global)<-[:R]-(fu:Fixe)-[:H]->(fd:Fixe)-[:R]->(gd:Global)<-[:R]-(md:Mobile)<-[:H]-(mu:Mobile)-[:R]->(gu2:Global) WHERE gu1 <> gu2 RETURN '{\"niveau\":\"'+labels(gu1)[1]+'\", \"libelle\":\"'+gu1.nom+'\", \"id\":\"'+gu1.id+'\", \"ecran\":\"'+labels(gu1)[0]+'\"},{\"niveau\":\"'+labels(fu)[1]+'\", \"libelle\":\"'+fu.nom+'\", \"id\":\"'+fu.id+'\", \"ecran\":\"'+labels(fu)[0]+'\"},{\"niveau\":\"'+labels(fd)[1]+'\", \"libelle\":\"'+fd.nom+'\", \"id\":\"'+fd.id+'\", \"ecran\":\"'+labels(fd)[0]+'\"},{\"niveau\":\"'+labels(gd)[1]+'\", \"libelle\":\"'+gd.nom+'\", \"id\":\"'+gd.id+'\", \"ecran\":\"'+labels(gd)[0]+'\"},{\"niveau\":\"'+labels(md)[1]+'\", \"libelle\":\"'+md.nom+'\", \"id\":\"'+md.id+'\", \"ecran\":\"'+labels(md)[0]+'\"},{\"niveau\":\"'+labels(mu)[1]+'\", \"libelle\":\"'+mu.nom+'\", \"id\":\"'+mu.id+'\", \"ecran\":\"'+labels(mu)[0]+'\"},{\"niveau\":\"'+labels(gu2)[1]+'\", \"libelle\":\"'+gu2.nom+'\", \"id\":\"'+gu2.id+'\", \"ecran\":\"'+labels(gu2)[0]+'\"}' as nodes, '{\"from\":\"'+fu.id+'\", \"to\":\"'+gu1.id+'\", \"relation\":\"R\"},{\"from\":\"'+fu.id+'\", \"to\":\"'+fd.id+'\", \"relation\":\"H\"},{\"from\":\"'+fd.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+md.id+'\", \"to\":\"'+gd.id+'\", \"relation\":\"R\"},{\"from\":\"'+mu.id+'\", \"to\":\"'+md.id+'\", \"relation\":\"H\"},{\"from\":\"'+mu.id+'\", \"to\":\"'+gu2.id+'\", \"relation\":\"R\"}' as edges"};
	
	public Exporter() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		Date date = new Date();
		for (int i = 0; i < queries.length; i++) {
			export(queries[i], file+(i+1)+"_"+dateFormat.format(date)+".txt");
		}
	}
	
	public void export(String query, String file) {
		result = Database.graphDb.execute(query);
		String s;
		String nodes = "";
		String edges = "";
		while (result.hasNext()) {
			Map<String, Object> map = result.next();
			nodes += map.get("nodes") + ",";
			edges += map.get("edges") + ",";
		}
		int nl = nodes.length();
		int el = edges.length();
		if (nl > 0)
			nodes = nodes.substring(0, nodes.length() - 1);
		if (el > 0)
			edges = edges.substring(0, edges.length() - 1);
		s = "{\"nodes\":["+nodes+"],\"edges\": ["+edges+"]}";
		try (PrintWriter writer = new PrintWriter(file)) {
			System.out.println(s);
			writer.write(s);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
