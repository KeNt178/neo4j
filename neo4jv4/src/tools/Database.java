package tools;

import java.io.File;
import java.util.Map;

import model.Child;
import model.Global;
import model.Parent;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public abstract class Database {
	public static final String DB_PATH = "D:/Documents/Stage/db/dbTest";
	public static final String INPUT_PATH = "resources/dicos";
	public static final String OUTPUT_PATH = "C:/Users/LAPOINQ/Desktop/output";
	
	public static final String[] ecrans = {"Fixe", "Mobile", "Global"};
	public static final String[] niveaux = {"Parent", "Brand", "Channel"};
	
	public static GraphDatabaseService graphDb;
	public static Transaction tx;
	public static int i;
	
	public static void build() {
		clean();
		start();
		createIndexes();
		
		//load Dico Fixe
		new Parent("Fixe");
		new Child("Fixe", "Brand", "Parent");
		new Child("Fixe", "Channel", "Brand");
		
		//load Dico Mobile
		new Parent("Mobile");
		new Child("Mobile", "Brand", "Parent");
		new Child("Mobile", "Brandsupport", "Brand");
		new Child("Mobile", "Channel", "Brandsupport");
		
		//take off Mobile Brandsupport level
		graphDb.execute("MATCH (b:Mobile:Brand)-[r1:H]->(bs:Mobile:Brandsupport)-[r2:H]->(c:Mobile:Channel) DELETE r1, r2 CREATE (b)-[:H]->(c)");
		graphDb.execute("MATCH (bs:Brandsupport)-[r]-() DELETE bs, r");
		
		//load rapprochements
		new Global();
		
		System.out.println("Db built");
	}
	
	public static void start() {
		System.out.print("Starting database... ");
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		i = 0;
		System.out.println("OK");
	}
	
	public static void clean() {
		System.out.print("Cleaning folder...");
		deleteDirectory(new File(DB_PATH));
		System.out.println("OK");
	}
	
	public static void beginTx() {
		tx = graphDb.beginTx();
	}
	
	public static void execute(String query, Map<String, Object> params) {
		graphDb.execute(query, params);
		if ((i = ++i%1000) == 0) {
			Database.restart();
		}
	}
	
	public static void commit() {
		tx.success();
		tx.close();
	}
	
	public static void restart() {
		commit();
		beginTx();
	}
	
	public static void createIndexes() {
		for (String d : ecrans) {
			createIndex(d);
		}
		for (String l : niveaux) {
			createIndex(l);
		}
	}
	
	public static void createIndex(String index) {
		beginTx();
		graphDb.execute("CREATE INDEX ON :"+index+"(id)");
		graphDb.execute("CREATE INDEX ON :"+index+"(nom)");
		commit();
	}
	
	public static boolean deleteDirectory(File directory) {
	    if(directory.exists()){
	        File[] files = directory.listFiles();
	        if(null!=files){
	            for(int i=0; i<files.length; i++) {
	                if(files[i].isDirectory()) {
	                    Database.deleteDirectory(files[i]);
	                }
	                else {
	                    files[i].delete();
	                }
	            }
	        }
	    }
	    return(directory.delete());
	}
}
