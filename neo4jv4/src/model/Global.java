package model;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.opencsv.CSVReader;

import tools.Database;

public class Global {
	String parentQuery = "MATCH (n:Parent {id:{p1}}) MERGE (g:Global:Parent {id:{p2}, nom:{p3}}) CREATE (n)-[:R]->(g)";
	String brandQuery = "MATCH (n:Brand {id:{p1}}) MERGE (g:Global:Brand {id:{p2}, nom:{p3}}) CREATE (n)-[:R]->(g)";
	String channelQuery = "MATCH (n:Channel {id:{p1}}) MERGE (g:Global:Channel {id:{p2}, nom:{p3}}) CREATE (n)-[:R]->(g)";
	int[] paramPlace = new int[]{4, 0, 1};
	String file = Database.INPUT_PATH+"/Rapprochements/exportDicoDams_20150601.csv";
	char sc = '�';
	
	public Global() {
		try (CSVReader csvr = new CSVReader(new FileReader(file), sc, '"', 1)) {
			String[] line;
			String query = "";
			int i = 0;
			
			Database.beginTx();
			while ((line = csvr.readNext()) != null) {
				switch (line[2]) {
				case "10": query = parentQuery; break;
				case "20": query = brandQuery; break;
				case "40": query = channelQuery; break;
				}
				Database.execute(query, fillParams(line));
				System.out.println(file+" "+i++);
			}
			Database.commit();
			csvr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(file+" treated");
	}
	
	public Map<String, Object> fillParams(String[] line) {
		Map<String, Object> params = new HashMap<String, Object>();
		for (int i = 0; i < paramPlace.length; i++)
			params.put("p"+(i+1), line[paramPlace[i]]);
		return params;
	}
}
