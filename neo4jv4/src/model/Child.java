package model;

import tools.Database;
import tools.Query;

public class Child {
	public Child(String ecran, String niveau, String niveauSup) {
		String squery = "MATCH (a:"+ecran+":"+niveauSup+" {id:{p1}}) CREATE (a)-[:H]->(:"+ecran+":"+niveau+" {id:{p2}, nom:{p3}})";
		int[] params = new int[]{2, 0, 1};
		String file = Database.INPUT_PATH+"/"+ecran+"/"+niveau+".csv";
		char sc = ',';
		
		new Query(squery, params, file, sc);
		System.out.println(":"+ecran+":"+niveau+" loaded");
	}
}
