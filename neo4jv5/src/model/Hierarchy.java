package model;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import org.neo4j.graphdb.Result;

import com.opencsv.CSVWriter;

import tools.Database;

public class Hierarchy {
	private Result result;
	private String[] headers;
	private int size;
	private String[] queries = {"MATCH (n:Global:Parent) return n.id as id, n.nom as label",
								"MATCH (n:Global:Brand)<-[:H]-(m:Global:Parent) return n.id as id, n.nom as label, m.id as parent_id",
								"MATCH (n:Global:Channel)<-[:H]-(m:Global:Brand) return n.id as id, n.nom as label, m.id as brand_id"};
	
	public Hierarchy() {
		String path = Database.OUTPUT_PATH+"/Global";
		new File(path).mkdirs();
		for (int i = 0; i < queries.length; i++) {
			loadQuery(queries[i]);
			exportResults(path+"/"+Database.niveaux[i]+".csv");
			System.out.println("Global"+Database.niveaux[i]+" generated");
		}
	}
	
	public void loadQuery(String query) {
		result = Database.graphDb.execute(query);
		size = result.columns().size();
		headers = result.columns().toArray(new String[size]);
	}
	
	public void exportResults(String file) {
		CSVWriter csvw;
		try {
			csvw = new CSVWriter(new FileWriter(file), ';');
			csvw.writeNext(result.columns().toArray(new String[size]));
			while (result.hasNext()) {
				String[] line = new String[size];
				Map<String, Object> map = result.next();
				for (int i = 0; i < size; i++)
						line[i] = (String) map.get(headers[i]);
				csvw.writeNext(line);
			}
			csvw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addResults(String file) {
		CSVWriter csvw;
		try {
			csvw = new CSVWriter(new FileWriter(file, true), ';');
			while (result.hasNext())
				csvw.writeNext(result.next().values().toArray(new String[size]));
			csvw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
