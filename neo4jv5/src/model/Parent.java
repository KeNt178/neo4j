package model;

import tools.Database;
import tools.Query;

public class Parent {
	public Parent(String ecran) {
		String query = "CREATE (:"+ecran+":Parent {id:{p1}, nom:{p2}})";
		int[] params = new int[]{0, 1};
		String file = Database.INPUT_PATH+"/"+ecran+"/Parent.csv";
		char splitChar = ',';
		
		new Query(query, params, file, splitChar);
		System.out.println(":"+ecran+":Parent imported");
	}
}
