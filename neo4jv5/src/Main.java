import model.Hierarchy;
import tools.Database;
import tools.Exporter;

public class Main {

	public static void main(String[] args) {
		Database.build();
		new Hierarchy();
		new Exporter();
	}

}
