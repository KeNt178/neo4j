package tools;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.opencsv.CSVReader;

public class Query {
	private int[] paramPlace;
	
	public Query(String query, int[] paramPlace, String file, char splitChar) {
		this.paramPlace = paramPlace;
		try (CSVReader csvr = new CSVReader(new FileReader(file), splitChar, '"', 1)) {
			String[] line;
			Database.beginTx();
			int i = 0;
			while ((line = csvr.readNext()) != null) {
				Database.execute(query, fillParams(line));
				if (i++%100000 == 99999)
					System.out.println(file+" "+i);
			}
			Database.commit();
			csvr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Map<String, Object> fillParams(String[] line) {
		Map<String, Object> params = new HashMap<String, Object>();
		for (int i = 0; i < paramPlace.length; i++)
			params.put("p"+(i+1), line[paramPlace[i]]);
		return params;
	}
}
