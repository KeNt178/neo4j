package factories;

import tools.Database;
import tools.Exporter;

public abstract class SingleQueryFactory {
	
	public static void exportIncoherences(String level) {
		Exporter exporter = new Exporter();
		
		String file1 = Database.OUTPUT_PATH+"/fusion/"+level+"_propositions.csv";
		String query1 = "MATCH (a:"+level+")-[:H]->(b)-[:R]->(g)<-[:R]-(c)<-[:H]-(d:"+level+") WHERE NOT ((a)-[:R]->() AND (d)-[:R]->()) AND id(a) < id(d) RETURN a.id as fusionner, a.nom, b.id, b.nom, g.id, g.nom, c.id, c.nom, d.id as avec, d.nom";
		exporter.loadQuery(query1);
		exporter.exportResults(file1);
		
		String file2 = Database.OUTPUT_PATH+"/incoherences/"+level+"_casserole.csv";
		String query2 = "MATCH (r)-[:R]->(g)<-[:R]-(a:"+level+")-[:H]->(b)-[:R]->(c)<-[:R]-(d)<-[:H]-(e:"+level+") WHERE NOT (e)-[:R]->() RETURN r.id, r.nom, g.id, g.nom, a.id, a.nom, b.id, b.nom, c.id, c.nom, d.id, d.nom, e.id, e.nom";
		exporter.loadQuery(query2);
		exporter.exportResults(file2);
		
		String file3 = Database.OUTPUT_PATH+"/incoherences/"+level+"_incisives.csv";
		String query3 = "MATCH (a:"+level+")-[:H]->(b)-[:R]->(g1)<-[:R]-(c)<-[:H]-(d:"+level+")-[:H]->(e)-[:R]->(g2)<-[:R]-(f)<-[:H]-(g:"+level+") WHERE id(a) < id(g) RETURN a.id, a.nom, b.id, b.nom, g1.id, g1.nom, c.id, c.nom, d.id, d.nom, e.id, e.nom, g2.id, g2.nom, f.id, f.nom, g.id, g.nom";
		exporter.loadQuery(query3);
		exporter.exportResults(file3);
		
		String file4 = Database.OUTPUT_PATH+"/incoherences/"+level+"_plaque.csv";
		String query4 = "MATCH (a:"+level+")-[:R]->(g1)<-[:R]-(b:"+level+")-[:R]->(g2)<-[:R]-(c:"+level+") RETURN a.id, a.nom, g1.id, g1.nom, b.id, b.nom, g2.id, g2.nom, c.id, c.nom";
		exporter.loadQuery(query4);
		exporter.exportResults(file4);
		
		//String query2 = "MATCH (pf:Fixe:"+level+")-[:CONTROLE]->(ff:Fixe)-[:FUSION]->(fg:Global)<-[:FUSION]-(fm:Mobile)<-[:CONTROLE]-(pm:Mobile) WHERE NOT (pf)-[:FUSION]->(:Global:"+level+") AND NOT (pm)-[:FUSION]->(:Global:"+level+") AND pf.nom = pm.nom RETURN pf.id, pf.nom, pm.id, pm.nom";
		//String ifile = Database.OUTPUT_PATH+"/fusion/"+level+"_i.csv";
		//String iquery = "MATCH (pf:Fixe:"+level+")-[:CONTROLE]->(ff:Fixe)-[:FUSION]->(fg:Global)<-[:FUSION]-(fm:Mobile)<-[:CONTROLE]-(pm:Mobile:"+level+") WHERE NOT (pf)-[:FUSION]->(:Global:"+level+") AND NOT (pm)-[:FUSION]->(:Global:"+level+") AND pf.nom <> pm.nom RETURN pf.id, pf.nom, pm.id, pm.nom";
	}
	
	public static void completionFusion(String level) {
		//creation global
		String query = "MATCH (a:"+level+":Fixe)-[:H]->(b)-[:R]->(c)<-[:R]-(d)<-[:H]-(e:"+level+":Mobile) WHERE NOT ((a)-[:R]->() AND (e)-[:R]->()) MERGE (a)-[:R]->(f:Global:"+level+")<-[:R]-(e) ON CREATE SET f.nom=e.nom, f.id=[str(9990000000"+((level.equals("Parent"))?"":"00")+"+toInt(a.id))]";
		//rapprochement x4
		String query2 = "MATCH (a:"+level+")-[r1:R]->(b:"+level+")<-[r2:R]-(c:"+level+"), (a)-[r3:R]->(d:"+level+")<-[r4:R]-(e:"+level+"), (a)-[r5:R]->(f:"+level+")<-[r6:R]-(g:"+level+") WHERE NOT (a.nom = 'WEB66' OR c.nom = 'WEB66' OR e.nom = 'WEB66' OR g.nom = 'WEB66') AND id(b) < id(d) AND id(d) < id(f) MERGE (h:Global:"+level+" {id:b.id+d.id+f.id, nom:b.nom}) MERGE (a)-[:R]->(h) MERGE (c)-[:R]->(h) MERGE (e)-[:R]->(h) MERGE (g)-[:R]->(h) DELETE b, d, f, r1, r2, r3, r4, r5, r6";
		//rapprochement x3 centre Fixe
		String query3 = "MATCH (a:"+level+":Fixe)-[r1:R]->(b:"+level+")<-[r2:R]-(c:"+level+"), (a)-[r3:R]->(d:"+level+")<-[r4:R]-(e:"+level+") WHERE NOT (a.nom = 'WEB66' OR ()<-[:R]-(c)-[:R]->() OR ()<-[:R]-(e)-[:R]->()) AND id(b) < id(d) MERGE (f:Global:"+level+" {id:b.id+d.id, nom:b.nom}) MERGE (a)-[:R]->(f) MERGE (c)-[:R]->(f) MERGE (e)-[:R]->(f) DELETE b, d, r1, r2, r3, r4";
		//rapprochement x3 centre Mobile
		String query4 = "MATCH (a:"+level+":Mobile)-[r1:R]->(b:"+level+")<-[r2:R]-(c:"+level+"), (a)-[r3:R]->(d:"+level+")<-[r4:R]-(e:"+level+") WHERE NOT (()<-[:R]-(c)-[:R]->() OR ()<-[:R]-(e)-[:R]->()) AND id(b) < id(d) MERGE (f:Global:"+level+" {id:b.id+d.id, nom:b.nom}) MERGE (a)-[:R]->(f) MERGE (c)-[:R]->(f) MERGE (e)-[:R]->(f) DELETE b, d, r1, r2, r3, r4";
		
		Database.graphDb.execute(query);
		Database.graphDb.execute(query2);
		Database.graphDb.execute(query3);
		Database.graphDb.execute(query4);
	}
	
	public static void completionFusionNom(String level) {
		String query = "MATCH (a:"+level+")<-[:H]-(b)-[:R]->(c)<-[:R]-(d)-[:H]->(e:"+level+":Mobile), (d)-[:H]->(f:"+level+":Mobile) WHERE e.nom = f.nom AND a.nom = e.nom MERGE (g:Global:"+level+") ON CREATE SET g.nom=a.nom, g.id=[str(999000000000+toInt(a.id))] MERGE (a)-[:R]->(g) MERGE (e)-[:R]->(g) MERGE (f)-[:R]->(g)";
		String query2 = "MATCH (a:"+level+")<-[:H]-(b)-[:R]->(c)<-[:R]-(d)-[:H]->(e:"+level+":Fixe), (d)-[:H]->(f:"+level+":Mobile) WHERE e.nom = f.nom AND a.nom = e.nom MERGE (g:Global:"+level+") MERGE (a)-[:R]->(g) MERGE (e)-[:R]->(g) MERGE (f)-[:R]->(g)";
		String query3 = "MATCH (a:"+level+")<-[:H]-(b)-[:R]->(c)<-[:R]-(d)-[:H]->(e:"+level+":Mobile) WHERE NOT (a)-[:R]->() AND a.nom = e.nom MERGE (a)-[:R]->(f:Global:"+level+")<-[:R]-(e) ON CREATE SET f.nom=a.nom, f.id=[str(999000000000+toInt(a.id))]";
		
		Database.graphDb.execute(query);
	}
	
	public static void generateHierarchy() {
		String query = "MATCH (pg1:Global)<-[:R]-(pf:Fixe)-[:H]->(ff:Fixe)-[:R]->(fg:Global)<-[:R]-(fm:Mobile)<-[:H]-(pm:Mobile)-[:R]->(pg2:Global) WHERE pg1 = pg2 CREATE (pg1)-[:H]->(fg)";
		Database.graphDb.execute(query);
	}
}
