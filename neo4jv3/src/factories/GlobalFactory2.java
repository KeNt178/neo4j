package factories;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.opencsv.CSVReader;

import tools.Database;

public class GlobalFactory2 {
	String parentQuery = "MATCH (n:Parent {id:{p1}}) MERGE (n)-[:R]->(g:Global:Parent {id:[{p2}], nom:{p3}})";
	String brandQuery = "MATCH (n:Brand {id:{p1}}) MERGE (n)-[:R]->(g:Global:Brand {id:[{p2}], nom:{p3}})";
	String brandSupQuery = "MATCH (n:Brandsupport {id:{p1}}) MERGE (n)-[:R]->(g:Global:Brandsupport {id:[{p2}], nom:{p3}})";
	int[] paramPlace = new int[]{4, 0, 1};
	String file = Database.INPUT_PATH+"/fusion/all.csv";
	char sc = '�';
	
	public GlobalFactory2() {
		try (CSVReader csvr = new CSVReader(new FileReader(file), sc, '"', 1)) {
			String[] line;
			String query = "";
			Database.beginTx();
			int i = 0;
			
			while ((line = csvr.readNext()) != null) {
				switch (line[2]) {
				case "10": query = parentQuery; break;
				case "20": query = brandQuery; break;
				case "40": query = brandSupQuery; break;
				}
				Database.execute(query, fillParams(line));
				System.out.println(file+" "+i++);
			}
				
			Database.commit();
			csvr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(file+" treated");
	}
	
	public Map<String, Object> fillParams(String[] line) {
		Map<String, Object> params = new HashMap<String, Object>();
		for (int i = 0; i < paramPlace.length; i++)
			params.put("p"+(i+1), line[paramPlace[i]]);
		return params;
	}
}
