package factories;

import tools.Database;
import tools.Query;

public class GlobalFactory extends NodeFactory {
	public GlobalFactory(String level) {
		String squery;
		if (level.equals("Brandsupport"))
			squery = "MERGE (g:Global:Brandsupport {id:[{p1}], nom:{p2}}) WITH g MATCH (m:Mobile {id:{p3}}) CREATE (m)-[:R]->(g) WITH g MATCH (f:Fixe {id:{p4}}) CREATE (f)-[:R]->(g)";
		else
			squery = "MERGE (g:Global:"+level+" {id:[{p1}], nom:{p2}}) WITH g MATCH (m:Mobile:"+level+" {id:{p3}}) CREATE (m)-[:R]->(g) WITH g MATCH (f:Fixe:"+level+" {id:{p4}}) CREATE (f)-[:R]->(g)";
		int[] params = new int[]{6, 7, 2, 0};
		String file = Database.INPUT_PATH+"/fusion/"+level+".csv";
		char sc = ';';
		
		query = new Query(squery, params, file, sc);
	}
}
