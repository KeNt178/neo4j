package factories;

import tools.Query;

public abstract class NodeFactory {
	protected Query query;
	
	public Query getQuery() {
		return query;
	}
}
