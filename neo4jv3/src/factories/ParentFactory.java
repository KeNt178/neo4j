package factories;

import tools.Database;
import tools.Query;

public class ParentFactory extends NodeFactory {	
	public ParentFactory(String device, String level) {
		String squery = "CREATE (:"+device+":"+level+" {id:{p1}, nom:{p2}})";
		int[] params = new int[]{0, 1};
		String file = Database.INPUT_PATH+"/"+device+"/"+level+".csv";
		char sc = ',';
		
		query = new Query(squery, params, file, sc);
		System.out.println(":"+device+":"+level+" loaded");
	}
}
