package factories;

import tools.Database;
import tools.Query;

public class ChildFactory extends NodeFactory {
	public ChildFactory(String device, String level) {
		String levelUp = "";
		switch (level) {
		case "Brand": levelUp = "Parent"; break;
		case "Brandsupport": levelUp = "Brand"; break;
		}
		String squery = "MATCH (a:"+device+":"+levelUp+" {id:{p1}}) CREATE (a)-[:H]->(:"+device+":"+level+" {id:{p2}, nom:{p3}})";
		int[] params = new int[]{2, 0, 1};
		String file = Database.INPUT_PATH+"/"+device+"/"+level+".csv";
		char sc = ',';
		
		query = new Query(squery, params, file, sc);
		System.out.println(":"+device+":"+level+" loaded");
	}
}
