package correction;

import tools.Database;
import tools.Exporter;

public class Rapprochement {
	public Rapprochement() {
		for (String s : Database.fusionnables)
			premiereIteration(s);
		
	}
	
	public void premiereIteration(String level) {
		Exporter exporter = new Exporter();
		
		String file = Database.OUTPUT_PATH+"/fusion/"+level+".csv";
		String query = "MATCH (a:Fixe:"+level+")-[:R]->(b:Global:"+level+")<-[:R]-(c:Mobile:"+level+") RETURN a.id as id_mnr, a.nom as nom_mnr, c.id as id_pim, c.nom as nom_pim, c.id as id_pipa, c.nom as nom_pipa, b.id as id_global, b.nom as nom_global";
		exporter.loadQuery(query);
		exporter.exportResults(file);
		
		/*String ifile = Database.OUTPUT_PATH+"/fusion/"+level+"_i.csv";
		String iquery = "MATCH (a:Fixe:"+level+")-[:R]->(b:Global:"+level+")<-[:R]-(c:Mobile:"+level+") WHERE NOT (replace(replace(replace(a.nom,' - TS OJD',''), ' - TS', ''), ' - T', '') = c.nom) RETURN a.id as id_mnr, a.nom as nom_mnr, c.id as id_pim, c.nom as nom_pim, c.id as id_pipa, c.nom as nom_pipa, b.id as id_global, a.nom+'/'+c.nom as nom_global";
		exporter.loadQuery(iquery, Database.fusion_headers);
		exporter.exportResults(ifile);*/
	}
}
