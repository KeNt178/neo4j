package tests;

import java.io.IOException;

import tools.Database;
import tools.DicoFixeGenerator;
import factories.SingleQueryFactory;

public class Main {
	public static void main(String[] args) throws IOException {
		//Database.build();
		//SingleQueryFactory.completionFusion("Brand");
		//SingleQueryFactory.completionFusion("Parent");
		//SingleQueryFactory.generateHierarchy();
		//SingleQueryFactory.exportIncoherences("Parent");
		//SingleQueryFactory.exportIncoherences("Brand");
		new DicoFixeGenerator();
		System.out.println("Db built");
	}
}
