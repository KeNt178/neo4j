package model;


public class Node {
	private String device;
	private String level;
	
	public Node(String device, String level) {
		this.device = device;
		this.level = level;
	}
	
	public Node(String level) {
		this.level = level;
	}
	
	public String getDevice() {
		return device;
	}
	
	public String getLevel() {
		return level;
	}
	
	public String toString() {
		return ":"+device+":"+level;
	}
}
