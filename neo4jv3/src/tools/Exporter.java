package tools;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.Result;
import org.parboiled.common.StringUtils;

import com.opencsv.CSVWriter;

public class Exporter {
	public Result result;
	private String[] headers;
	private int size;
	
	public Exporter() {}
	
	public void loadQuery(String query) {
		result = Database.graphDb.execute(query);
		size = result.columns().size();
		headers = result.columns().toArray(new String[size]);
	}
	
	public void exportResults(String file) {
		CSVWriter csvw;
		try {
			csvw = new CSVWriter(new FileWriter(file), ';');
			csvw.writeNext(result.columns().toArray(new String[size]));
			while (result.hasNext()) {
				String[] line = new String[size];
				Map<String, Object> map = result.next();
				for (int i = 0; i < size; i++) {
					if (map.get(headers[i]).getClass().toString().equals("class [Ljava.lang.String;")) {
						line[i] = "";
						int l = ((String[]) map.get(headers[i])).length;
						for (int j = 0; j < l; j++)
							line[i] += ((String[]) map.get(headers[i]))[j]+";";
						line[i] = line[i].substring(0, line[i].length() - 1);
					} else {
						line[i] = (String) map.get(headers[i]);
					}
				}
				csvw.writeNext(line);
			}
			csvw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addResults(String file) {
		CSVWriter csvw;
		try {
			csvw = new CSVWriter(new FileWriter(file, true), ';');
			while (result.hasNext())
				csvw.writeNext(result.next().values().toArray(new String[size]));
			csvw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
