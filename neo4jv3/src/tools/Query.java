package tools;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.opencsv.CSVReader;

public class Query {
	private String query;
	private int[] paramPlace;
	private String file;
	private char splitChar;
	
	public Query(String query, int[] paramPlace, String file, char splitChar) {
		this.query = query;
		this.paramPlace = paramPlace;
		this.file = file;
		this.splitChar = splitChar;
	}
	
	public void run() {
		try (CSVReader csvr = new CSVReader(new FileReader(file), splitChar, '"', 1)) {
			String[] line;
			Database.beginTx();
			int i = 0;
			while ((line = csvr.readNext()) != null) {
				Database.execute(query, fillParams(line));
				if (i++%10000 == 0)
					System.out.println(file+" "+i);
			}
			Database.commit();
			csvr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(file+" treated");
	}
	
	public Map<String, Object> fillParams(String[] line) {
		Map<String, Object> params = new HashMap<String, Object>();
		for (int i = 0; i < paramPlace.length; i++)
			params.put("p"+(i+1), line[paramPlace[i]]);
		return params;
	}
}
