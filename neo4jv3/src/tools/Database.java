package tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

import factories.ChildFactory;
import factories.GlobalFactory2;
import factories.NodeFactory;
import factories.ParentFactory;

public abstract class Database {
	public static final String DB_PATH = "D:/Documents/Stage/db/javaDb1";
	
	public static final String INPUT_PATH = "resources/dicos";
	public static final String OUTPUT_PATH = "C:/Users/LAPOINQ/Desktop/output";
	
	public static final String[] devices = {"Fixe", "Mobile", "Global"};
	public static final String[] levels = {"Parent", "Brand", "Brandsupport", "Channel"};
	
	public static final String[] children = {"Brand", "Brandsupport", "Channel"};
	public static final String[] parents = {"Parent"};
	
	public static final String[] fusionnables = {"Parent", "Brand", "Brandsupport"};
	
	public static final String[] fusion_headers = {"id_mnr", "nom_mnr", "id_pim", "nom_pim", "id_pipa", "nom_pipa", "id_global", "nom_global"};

	public static GraphDatabaseService graphDb;
	public static Transaction tx;
	public static int i;
	
	public static void start() {
		System.out.print("Starting database... ");
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		Indexer.createIndexes();
		i = 0;
		System.out.println("OK");
	}
	
	public static void clean() {
		System.out.print("Cleaning folder...");
		deleteDirectory(new File(DB_PATH));
		System.out.println("OK");
	}
	
	public static void beginTx() {
		tx = graphDb.beginTx();
	}
	
	public static void execute(String query, Map<String, Object> params) {
		graphDb.execute(query, params);
		if ((i = ++i%1000) == 0) {
			Database.restart();
		}
	}
	
	public static void commit() {
		tx.success();
		tx.close();
	}
	
	public static void restart() {
		commit();
		beginTx();
	}
	
	public static boolean deleteDirectory(File directory) {
	    if(directory.exists()){
	        File[] files = directory.listFiles();
	        if(null!=files){
	            for(int i=0; i<files.length; i++) {
	                if(files[i].isDirectory()) {
	                    Database.deleteDirectory(files[i]);
	                }
	                else {
	                    files[i].delete();
	                }
	            }
	        }
	    }
	    return(directory.delete());
	}
	
	public static void copyFileUsingFileChannels(File source, File dest) throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}
	
	public static void build() {
		List<NodeFactory> nfs = new ArrayList<NodeFactory>();
		
		for (int i = 0; i < devices.length - 1; i++)
			for (int j = 0; j < parents.length; j++)
				nfs.add(new ParentFactory(devices[i], parents[j]));
		
		for (int i = 0; i < devices.length - 1; i++)
			for (int j = 0; j < children.length - 1; j++)
				nfs.add(new ChildFactory(devices[i], children[j]));
		
		/*for (int i = 0; i < fusionnables.length; i++)
			nfs.add(new GlobalFactory(fusionnables[i]));*/
		

		clean();
		start();
		for (NodeFactory nf : nfs)
			nf.getQuery().run();
		//new GlobalFactory2();
	}
}
