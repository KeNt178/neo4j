package tools;

public class Indexer {
	public static void createIndexes() {
		for (String d : Database.devices) {
			createIndex(d);
		}
		for (String l : Database.levels) {
			createIndex(l);
		}
	}
	
	public static void createIndex(String index) {
		Database.beginTx();
		Database.graphDb.execute("CREATE INDEX ON :"+index+"(id)");
		Database.graphDb.execute("CREATE INDEX ON :"+index+"(nom)");
		Database.commit();
	}
}
