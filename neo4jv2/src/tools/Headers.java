package tools;

public final class Headers {
	public static String[] PARENT = {"id", "label"};
	public static String[] BRAND = {"id", "label", "parent_id"};
	public static String[] SUPPORTBRAND = {"id", "label", "brand_id"};
	public static String[] CHANNEL = {"id", "label", "supportBrand_id"};
}
