package tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;

public class Database {
	public static final String DB_PATH = "D:/Documents/Stage/db/javaDb1";

	public static String[] devices = {"Fixe", "Mobile", "Global"};
	public static String[] levels = {"Parent", "Brand", "SupportBrand", "Channel"};
	
	public static final String FIXE_PATH = "resources/dicos/fixe/";
	public static final String MOBILE_PATH = "resources/dicos/mobile/";
	public static final String FIXE_PARENTS = FIXE_PATH + "FixeParents.csv";
	public static final String FIXE_BRANDS = FIXE_PATH + "FixeBrands.csv";
	public static final String FIXE_SUPPORTBRANDS = FIXE_PATH + "FixeSupportBrands.csv";
	public static final String FIXE_CHANNELS = FIXE_PATH + "FixeChannels.csv";
	public static final String MOBILE_PARENTS = MOBILE_PATH + "MobileParents.csv";
	public static final String MOBILE_BRANDS = MOBILE_PATH + "MobileBrands.csv";
	public static final String MOBILE_SUPPORTBRANDS = MOBILE_PATH + "MobileSupportBrands.csv";
	public static final String MOBILE_CHANNELS = MOBILE_PATH + "MobileChannels.csv";
	
	public static final String OUT_FIXE_PATH = "C:/Users/LAPOINQ/Desktop/output/fixe/";
	public static final String OUT_MOBILE_PATH = "C:/Users/LAPOINQ/Desktop/output/mobile/";
	public static final String OUT_GLOBAL_PATH = "C:/Users/LAPOINQ/Desktop/output/global/";
	public static final String OUT_FIXE_PARENTS = OUT_FIXE_PATH + "FixeParents.csv";
	public static final String OUT_FIXE_BRANDS = OUT_FIXE_PATH + "FixeBrands.csv";
	public static final String OUT_FIXE_SUPPORTBRANDS = OUT_FIXE_PATH + "FixeSupportBrands.csv";
	public static final String OUT_FIXE_CHANNELS = OUT_FIXE_PATH + "FixeChannels.csv";
	public static final String OUT_MOBILE_PARENTS = OUT_MOBILE_PATH + "MobileParents.csv";
	public static final String OUT_MOBILE_BRANDS = OUT_MOBILE_PATH + "MobileBrands.csv";
	public static final String OUT_MOBILE_SUPPORTBRANDS = OUT_MOBILE_PATH + "MobileSupportBrands.csv";
	public static final String OUT_MOBILE_CHANNELS = OUT_MOBILE_PATH + "MobileChannels.csv";
	public static final String OUT_GLOBAL_PARENTS = OUT_GLOBAL_PATH + "GlobalParents.csv";
	public static final String OUT_GLOBAL_BRANDS = OUT_GLOBAL_PATH + "GlobalBrands.csv";
	public static final String OUT_GLOBAL_SUPPORTBRANDS = OUT_GLOBAL_PATH + "GlobalSupportBrands.csv";
	public static final String OUT_GLOBAL_CHANNELS = OUT_GLOBAL_PATH + "GlobalChannels.csv";
	
	public static final String IG_COMPUTER = "resources/txt/IG_computer.txt";
	public static final String IG_MOBILE = "resources/txt/IG_mobile.txt";
	public static final String CODES_PARENTS = "resources/csv/codes_parents.csv";
	public static final String CODES_BRANDS = "resources/csv/codes_brands.csv";
	
	public static final String OUT_CODES_PARENTS = "C:/Users/LAPOINQ/Desktop/output/codes_parents.csv";
	public static final String OUT_CODES_BRANDS = "C:/Users/LAPOINQ/Desktop/output/codes_brands.csv";
	
	public static GraphDatabaseService graphDb;
	public static Transaction tx;
	public static int i;
	
	public static void start() {
		System.out.print("Starting database... ");
		graphDb = new GraphDatabaseFactory().newEmbeddedDatabase(DB_PATH);
		i = 0;
		System.out.println("OK");
	}
	
	public static void beginTx() {
		tx = graphDb.beginTx();
	}
	
	public static void execute(String query, String[] paramList) {
		Map<String, Object> params = new HashMap<String, Object>();
		for (int i = 0; i < paramList.length; i++) {
			params.put("p"+(i+1), paramList[i]);
		}
		graphDb.execute(query, params);
		if ((i = ++i%1000) == 0) {
			Database.restart();
		}
	}
	
	public static void commit() {
		tx.success();
		tx.close();
	}
	
	public static void restart() {
		commit();
		beginTx();
	}
	
	public static boolean deleteDirectory(File directory) {
	    if(directory.exists()){
	        File[] files = directory.listFiles();
	        if(null!=files){
	            for(int i=0; i<files.length; i++) {
	                if(files[i].isDirectory()) {
	                    Database.deleteDirectory(files[i]);
	                }
	                else {
	                    files[i].delete();
	                }
	            }
	        }
	    }
	    return(directory.delete());
	}
	
	public static void copyFileUsingFileChannels(File source, File dest) throws IOException {
		FileChannel inputChannel = null;
		FileChannel outputChannel = null;
		try {
			inputChannel = new FileInputStream(source).getChannel();
			outputChannel = new FileOutputStream(dest).getChannel();
			outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
		} finally {
			inputChannel.close();
			outputChannel.close();
		}
	}
}
