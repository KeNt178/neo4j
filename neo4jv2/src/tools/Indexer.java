package tools;

public class Indexer {
	public static void createIndexes() {
		for (int i = 0; i < Database.devices.length; i++) {
			createIndex(Database.devices[i]);
		}
		for (int i = 0; i < Database.levels.length; i++) {
			createIndex(Database.levels[i]);
		}
	}
	
	public static void createIndex(String index) {
		Database.beginTx();
		Database.graphDb.execute("CREATE INDEX ON :"+index+"(id)");
		Database.commit();
	}
}
