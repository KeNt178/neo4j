package tools;

import java.util.Map;

import org.neo4j.graphdb.Result;

public class Executor {
	private Result result;
	private Map<String,Object> row;
	
	public Executor(String query) {
		result = Database.graphDb.execute(query);
	}
	
	public Map<String,Object> nextRow() {
		while (result.hasNext()) {
			return result.next();
		}
		return null;
	}
	
	public String getValue(String key) {
		return (String) row.get(key);
	}
	
}
