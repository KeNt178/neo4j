package tools;

import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;


public class CsvReaderPerso extends CSVReader {
	
	public CsvReaderPerso(String file, char splitChar) throws IOException {
		super(new FileReader(file), splitChar, '"', 1);
		//super(new FileReader(file), '\t');
	}

	public String[] getNextLine() throws IOException {
		String[] line;
		if ((line = readNext()) != null)
			return line;
		return null;
	}
}
