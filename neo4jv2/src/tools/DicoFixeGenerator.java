package tools;

import java.io.FileWriter;
import java.io.IOException;

import com.opencsv.CSVWriter;

public class DicoFixeGenerator {
	
	private CsvReaderPerso csvr;
	private String[] line;
	
	public DicoFixeGenerator() throws IOException {
		csvr = new CsvReaderPerso(Database.IG_COMPUTER, '\t');
		line = csvr.getNextLine();
		parents();
		brands();
		supportBrands();
		channels();
		csvr.close();
	}

	private void channels() throws IOException {
		CSVWriter csvw = new CSVWriter(new FileWriter("C:/Users/LAPOINQ/Desktop/FixeChannels.csv"), ',', '"');
		csvw.writeNext(Headers.SUPPORTBRAND);
		String[] entries;
		do {
			if (line[4].equals("")) {
				entries = (line[5]+"�"+line[6]+"�"+line[2]).split("�");
			} else {
				entries = (line[5]+"�"+line[6]+"�"+line[4]).split("�");
			}
			csvw.writeNext(entries);
		} while ((line = csvr.getNextLine()) != null);
		csvw.close();
	}

	private void supportBrands() throws IOException {
		CSVWriter csvw = new CSVWriter(new FileWriter("C:/Users/LAPOINQ/Desktop/FixeSupportBrands.csv"), ',', '"');
		csvw.writeNext(Headers.SUPPORTBRAND);
		do {
			String[] entries = (line[4]+"�"+line[6]+"�"+line[2]).split("�");
			csvw.writeNext(entries);
			line = csvr.getNextLine();
		} while (line[5].equals(""));
		csvw.close();
	}

	private void brands() throws IOException {
		CSVWriter csvw = new CSVWriter(new FileWriter("C:/Users/LAPOINQ/Desktop/FixeBrands.csv"), ',', '"');
		csvw.writeNext(Headers.BRAND);
		do {
			String[] entries = (line[2]+"�"+line[6]+"�"+line[1]).split("�");
			csvw.writeNext(entries);
			line = csvr.getNextLine();
		} while (line[4].equals(""));
		csvw.close();
	}

	private void parents() throws IOException {
		CSVWriter csvw = new CSVWriter(new FileWriter("C:/Users/LAPOINQ/Desktop/FixeParents.csv"), ',', '"');
		csvw.writeNext(Headers.PARENT);
		do {
			String[] entries = (line[1]+"�"+line[6]).split("�");
			csvw.writeNext(entries);
			line = csvr.getNextLine();
		} while (line[2].equals(""));
		csvw.close();
	}
}
