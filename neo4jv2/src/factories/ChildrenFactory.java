package factories;

import java.io.IOException;

import tools.CsvReaderPerso;
import tools.Database;

public class ChildrenFactory {
	private String device;
	private String level;
	private String file;
	private String query;
	
	public ChildrenFactory(String device, String level) {
		this.device = device;
		this.level = level;                      
		setQuery();
		setFile();
		createChildren();
	}
	
	public void createChildren(){
		try (CsvReaderPerso csvr = new CsvReaderPerso(file, ',')) {
			String[] line;
			Database.beginTx();
			while ((line = csvr.getNextLine()) != null)
				Database.execute(query, fillParams(line));
			Database.commit();
			csvr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(device+level+"s loaded");
	}
	
	public void setFile() {
		switch (device) {
		case "Fixe":
			switch (level) {
			case "Parent": file = Database.FIXE_PARENTS; break;
			case "Brand": file = Database.FIXE_BRANDS; break;
			case "SupportBrand": file = Database.FIXE_SUPPORTBRANDS; break;
			case "Channel": file = Database.FIXE_CHANNELS; break;
			}
		break;
		case "Mobile":
			switch (level) {
			case "Parent": file = Database.MOBILE_PARENTS; break;
			case "Brand": file = Database.MOBILE_BRANDS; break;
			case "SupportBrand": file = Database.MOBILE_SUPPORTBRANDS; break;
			case "Channel": file = Database.MOBILE_CHANNELS; break;
			}
		break;
		}
	}
	
	public void setQuery() {
		switch (level) {
		case "Parent": query = "CREATE (:"+device+":"+level+" {id:{p1}, nom:{p2}})"; break;
		case "Brand":
		case "SupportBrand":
		case "Channel": query = "MATCH (a:"+device+" {id:{p1}}) CREATE (a)-[:CONTROLE]->(:"+device+":"+level+" {id:{p2}, nom:{p3}})"; break;
		}
	}
	
	public String[] fillParams(String[] line) {
		String[] params = null;
		switch (level) {
		case "Parent": params = new String[]{line[0], line[1]}; break;
		case "Brand":
		case "SupportBrand":
		case "Channel": params = new String[]{line[2], line[0], line[1]}; break;
		}
		return params;
	}
}
