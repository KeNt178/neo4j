package factories;

import java.io.IOException;

import tools.Database;

public class DevicesFactory {	
	public DevicesFactory(String device) throws IOException {
		for (int i = 0; i < Database.levels.length; i++)
			new ChildrenFactory(device, Database.levels[i]);
	}
}
