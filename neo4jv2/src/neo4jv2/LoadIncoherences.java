package neo4jv2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;

import com.opencsv.CSVWriter;

import tools.Database;
import tools.Executor;

public class LoadIncoherences {	
	public LoadIncoherences() throws IOException {
		generateFusionFiles("Parent");
		generateFusionFiles("Brand");
		//wrongParentMobile();
		//wrongParentFixe();
		//notSymetric();
	}
	
	public void generateFusionFiles(String level) throws IOException {
		String file = null;
		switch (level) {
		case "Parent": file = Database.OUT_CODES_PARENTS; break;
		case "Brand": file = Database.OUT_CODES_BRANDS; break;
		}
		CSVWriter csvw = new CSVWriter(new FileWriter(""), ';');
		csvw.writeNext("id_mnr;nom_mnr;id_pim;nom_pim;id_pipa;nom_pipa;id_global;nom_global".split(";"));
		String query = "MATCH (f:Fixe:"+level+")-[:FUSION]->(g:Global:"+level+")<-[:FUSION]-(m:Mobile:"+level+") RETURN g.id, f.id, f.nom, m.id, m.nom";
		System.out.print("Calcul des resultats... ");
		Database.beginTx();
		Executor exec = new Executor(query);
		System.out.println("OK");
		Map<String,Object> row;
		while ((row = exec.nextRow()) != null)
	    {
	        String[] line = {(String) row.get("f.id"), (String) row.get("f.nom"), (String) row.get("m.id"), (String) row.get("m.nom"), (String) row.get("m.id"), (String) row.get("m.nom"), (String) row.get("g.id"), (String) row.get("f.nom")/*+"/"+row.get("m.nom")*/};
	        csvw.writeNext(line);
        }
		Database.commit();
		
		query = "MATCH (pf:Fixe:"+level+")-[:CONTROLE]->(ff:Fixe)-[:FUSION]->(fg:Global)<-[:FUSION]-(fm:Mobile)<-[:CONTROLE]-(pm:Mobile) WHERE NOT (pf)-[:FUSION]->(:Global:"+level+") AND NOT (pm)-[:FUSION]->(:Global:"+level+") AND pf.nom = pm.nom RETURN pf.id, pf.nom, pm.id, pm.nom";
		System.out.print("Calcul des resultats... ");
		Database.beginTx();
		exec = new Executor(query);
		System.out.println("OK");
		while ((row = exec.nextRow()) != null)
	    {
	        int nbr = 7 - ((String) row.get("pf.id")).length();
	        String zeros = "";
	        for (int i = 0; i < nbr; i++) {
	        	zeros += "0";
	        }
	        String[] line = {(String) row.get("pf.id"), (String) row.get("pf.nom"), (String) row.get("pm.id"), (String) row.get("pm.nom"), (String) row.get("pm.id"), (String) row.get("pm.nom"), "999"+zeros+row.get("pf.id"), (String) row.get("pf.nom")/*+"/"+row.get("pm.nom")*/};
	        csvw.writeNext(line);
        }
		csvw.close();
		Database.commit();
		
		String ifile = file.substring(0, file.length() - 4) + "_i" + file.substring(file.length() - 4);
		FileWriter ifw = new FileWriter(ifile);
		CSVWriter icsvw = new CSVWriter(ifw, ';');
		icsvw.writeNext("id_mnr;nom_mnr;id_pim;nom_pim;id_pipa;nom_pipa;id_global;nom_global".split(";"));
		query = "MATCH (pf:Fixe:"+level+")-[:CONTROLE]->(ff:Fixe)-[:FUSION]->(fg:Global)<-[:FUSION]-(fm:Mobile)<-[:CONTROLE]-(pm:Mobile:"+level+") WHERE NOT (pf)-[:FUSION]->(:Global:"+level+") AND NOT (pm)-[:FUSION]->(:Global:"+level+") AND pf.nom <> pm.nom RETURN pf.id, pf.nom, pm.id, pm.nom";
		System.out.print("Calcul des resultats... ");
		Database.beginTx();
		exec = new Executor(query);
		System.out.println("OK");
		while ((row = exec.nextRow()) != null)
	    {
			int nbr = 7 - ((String) row.get("pf.id")).length();
	        String zeros = "";
	        for (int i = 0; i < nbr; i++) {
	        	zeros += "0";
	        }
			String[] line = {(String) row.get("pf.id"), (String) row.get("pf.nom"), (String) row.get("pm.id"), (String) row.get("pm.nom"), (String) row.get("pm.id"), (String) row.get("pm.nom"), "999"+zeros+row.get("pf.id"), (String) row.get("pf.nom")+"/"+row.get("pm.nom")};
	        icsvw.writeNext(line);
	    }
		icsvw.close();
		Database.commit();
	}
	
	public void wrongParentMobile() throws FileNotFoundException {
		File out = new File("C:/Users/LAPOINQ/Desktop/wrong_parentmobile.csv");
		Database.beginTx();
		String query = "MATCH (pmtrue:ParentMobile)-[:FUSION]->(pg:ParentGlobal)<-[:FUSION]-(pf:ParentFixe)-[:CONTROLE]->(bf:BrandFixe)-[:FUSION]->(bg:BrandGlobal)<-[:FUSION]-(bm:BrandMobile)<-[:CONTROLE]-(pm:ParentMobile) WHERE NOT (pm)-[:FUSION]->(:ParentGlobal) RETURN pm.id, pmtrue.id, bm.id, bm.nom";
		System.out.print("Calcul des resultats... ");
		Executor exec = new Executor(query);
		System.out.println("OK");
		PrintStream ps = new PrintStream(out);
		ps.println("ACTION;PARENT_ID;BRAND_ID;NAME");
		Map<String,Object> row;
		while ((row = exec.nextRow()) != null)
	    {
        	ps.println("del;"+row.get("pm.id")+";"+row.get("bm.id")+";"+row.get("bm.nom"));
        	ps.println("add;"+row.get("pmtrue.id")+";"+row.get("bm.id")+";"+row.get("bm.nom"));
        }
		ps.close();
		Database.commit();
	}
	
	public void wrongParentFixe() throws FileNotFoundException {
		File out = new File("C:/Users/LAPOINQ/Desktop/wrong_parentfixe.csv");
		Database.beginTx();
		String query ="MATCH (pf:ParentFixe)-[:CONTROLE]->(bf:BrandFixe)-[:FUSION]->(bg:BrandGlobal)<-[:FUSION]-(bm:BrandMobile)<-[:CONTROLE]-(pm:ParentMobile)-[:FUSION]->(pg:ParentGlobal)<-[:FUSION]-(pftrue:ParentFixe) WHERE NOT (pf)-[:FUSION]->(:ParentGlobal) RETURN pf.id, pftrue.id, bf.id, bf.nom";
		System.out.print("Calcul des resultats... ");
		Executor exec = new Executor(query);
		System.out.println("OK");
		PrintStream ps = new PrintStream(out);
		ps.println("ACTION;PARENT_ID;BRAND_ID;NAME");
		Map<String,Object> row;
		while ((row = exec.nextRow()) != null)
	    {
        	ps.println("del;"+row.get("pf.id")+";"+row.get("bf.id")+";"+row.get("bf.nom"));
        	ps.println("add;"+row.get("pftrue.id")+";"+row.get("bf.id")+";"+row.get("bf.nom"));
        }
		ps.close();
		Database.commit();
	}
	
	public void notSymetric() throws FileNotFoundException {
		File out = new File("C:/Users/LAPOINQ/Desktop/not_symetric.csv");
		Database.beginTx();
		String query = "MATCH (bf:BrandFixe)<-[:CONTROLE]-(pf:ParentFixe)-[:FUSION]->(pg:ParentGlobal)<-[:FUSION]-(pm:ParentMobile)-[:CONTROLE]->(bm:BrandMobile) RETURN bm.nom, collect(bf.nom) as bfnoms limit 1";
		System.out.print("Calcul des resultats... ");
		Executor exec = new Executor(query);
		System.out.println("OK");
		PrintStream ps = new PrintStream(out);
		Map<String,Object> row;
		while ((row = exec.nextRow()) != null)
	    {
        	System.out.println(row.get("bfnoms").toString());
        }
		ps.close();
		Database.commit();
	}
}
