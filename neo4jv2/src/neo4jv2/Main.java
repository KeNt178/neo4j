package neo4jv2;

import java.io.File;
import java.io.IOException;

import factories.DevicesFactory;
import tools.Database;
//import tools.DicoFixeGenerator;
import tools.Indexer;

public class Main {
	
	public static void main(String[] args) throws IOException {
		/*new DicoFixeGenerator();*/
		/*Database.deleteDirectory(new File(Database.DB_PATH));
		
		Indexer.createIndexes();
		new DevicesFactory("Fixe");
		new DevicesFactory("Mobile");
		new FusionFactory("Parent");
		new FusionFactory("Brand");
		System.out.println("DB built");
		new LoadGlobalParents();
		new LoadGlobalBrands();*/
		Database.start();
		//new LoadIncoherences();
		new GenerateHierarchy();
		//Database.copyFileUsingFileChannels(new File(Database.FIXE_BRANDS), new File("C:/Users/LAPOINQ/Desktop/FixeChannels.csv"));
	}
}
