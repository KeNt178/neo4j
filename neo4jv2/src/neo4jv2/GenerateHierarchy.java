package neo4jv2;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

import com.opencsv.CSVWriter;

import tools.Database;
import tools.Executor;
import tools.Headers;

public class GenerateHierarchy {
	public GenerateHierarchy() throws IOException {
		Parents();
		for (int i = 1; i < Database.levels.length; i++)
			Global(Database.levels[i]);
	}
	
	private void Parents() throws IOException {
		String file = Database.OUT_GLOBAL_PARENTS;
		String[] headers = Headers.PARENT;
		CSVWriter csvw = new CSVWriter(new FileWriter(file), ',', '"');
		csvw.writeNext(headers);
		String query = "MATCH (g:Global:Parent) return g.id, g.nom";
		System.out.print("Calcul des resultats... ");
		Database.beginTx();
		Executor exec = new Executor(query);
		System.out.println("OK");
		Map<String,Object> row;
		while ((row = exec.nextRow()) != null)
	    {
	        String[] line = {(String) row.get("g.id"), (String) row.get("g.nom")};
	        csvw.writeNext(line);
        }
		csvw.close();
		Database.commit();
	}

	private void Global(String level) throws IOException {
		String file = null;
		String[] headers = null;
		switch (level) {
		case "Brand": file = Database.OUT_GLOBAL_BRANDS; headers = Headers.BRAND; break;
		case "SupportBrand": file = Database.OUT_GLOBAL_SUPPORTBRANDS; headers = Headers.SUPPORTBRAND; break;
		case "Channel": file = Database.OUT_GLOBAL_CHANNELS; headers = Headers.CHANNEL; break;
		}
		CSVWriter csvw = new CSVWriter(new FileWriter(file), ',', '"');
		csvw.writeNext(headers);
		String query = "MATCH (pg1:Global)<-[:FUSION]-(pf:Fixe)-[:CONTROLE]->(ff:Fixe:"+level+")-[:FUSION]->(fg:Global:"+level+")<-[:FUSION]-(fm:Mobile:"+level+")<-[:CONTROLE]-(pm:Mobile)-[:FUSION]->(pg2:Global) WHERE pg1 = pg2 RETURN pg1.id, fg.id, fg.nom";
		System.out.print("Calcul des resultats... ");
		Database.beginTx();
		Executor exec = new Executor(query);
		System.out.println("OK");
		Map<String,Object> row;
		while ((row = exec.nextRow()) != null)
	    {
	        String[] line = {(String) row.get("fg.id"), (String) row.get("fg.nom"), (String) row.get("pg1.id")};
	        csvw.writeNext(line);
        }
		csvw.close();
		Database.commit();
	}
}
