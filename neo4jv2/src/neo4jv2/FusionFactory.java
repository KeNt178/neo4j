package neo4jv2;

import java.io.IOException;

import tools.CsvReaderPerso;
import tools.Database;

public class FusionFactory {
	private String level;
	private String file;
	private String query;
	
	public FusionFactory(String level) throws IOException {
		this.level = level;
		query = "MATCH (pf:Fixe:"+level+" {id:{p1}}), (pm:Mobile:"+level+" {id:{p2}}) CREATE (pg:Global:"+level+" {id:{p3}, nom:{p4}}), (pf)-[:FUSION]->(pg)<-[:FUSION]-(pm)";
		setFile();
		createGlobals();
	}
	
	public void createGlobals() {
		try (CsvReaderPerso csvr = new CsvReaderPerso(file, ';')) {
			String[] line;
			Database.beginTx();
			while ((line = csvr.getNextLine()) != null)
				Database.execute(query, new String[]{line[0], line[2], line[6], line[7]});
			Database.commit();
			csvr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Global"+level+"s loaded");
	}
	
	public void setFile() {
		switch (level) {
			case "Parent": file = Database.CODES_PARENTS; break;
			case "Brand": file = Database.CODES_BRANDS; break;
			//case "SupportBrand": file = Database.FIXE_SUPPORTBRANDS; break;
			//case "Channel": file = Database.FIXE_CHANNELS; break;
		}
	}
}
