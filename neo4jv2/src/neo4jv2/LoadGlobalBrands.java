package neo4jv2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import tools.CsvReaderPerso;
import tools.Database;

public class LoadGlobalBrands {
	public LoadGlobalBrands() throws IOException {
		CsvReaderPerso csvr = new CsvReaderPerso(Database.CODES_BRANDS, ';');
		String[] line;
		String query;
        Map<String, Object> params = new HashMap<String, Object>();
		
        Database.beginTx();
		int i = 0;

		while ((line = csvr.getNextLine()) != null) {
			query = "MATCH (bf:BrandFixe {id:{idf}}), (bm:BrandMobile {id:{idm}}) CREATE (bg:BrandGlobal {id:{idg}, nom:{nom}}), (bf)-[:FUSION]->(bg)<-[:FUSION]-(bm)";
        	params.put("idf", line[0]);
        	params.put("idm", line[2]);
        	params.put("idg", line[6]);
        	params.put("nom", line[7]);
        	Database.graphDb.execute(query, params);
        	if (++i%1000 == 0) {
        		System.out.println(i+" Merged Brand : "+line[7]+" ("+line[6]+") with Fixe ("+line[0]+") and Mobile ("+line[2]+")");
        		Database.restart();
			}
		}
		Database.commit();
		csvr.close();
        System.out.println("Brands merged");
	}
}
